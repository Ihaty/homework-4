package Test;

import com.company.MathHW;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class TestMath {
    private static MathHW task;

    @BeforeAll
    static void setUp() {
        task = new MathHW();
    }

    @Test
    void task1() {
        int expected = (int) 3.49;
        double actual = task.task1(36, 10);
        assertEquals(expected, actual);
    }

    @Test
    void task1_2() {
        int expected = (int) 3.49;
        double actual = task.task1_2(36, 0.174533);
        assertEquals(expected, actual);
    }

    @Test
    void task2() {
        int expected = 45;
        int actual = task.task2(10, 10, 5, 2);
        assertEquals(expected, actual);
    }

    @Test
    void task3() {
        boolean expected = false;
        boolean actual = task.task3(1.5, 0.4);
        assertEquals(expected, actual);
    }

    @Test
    void task4() {
        double expected = 5.409809784343256;
        double actual = task.task4();
        assertEquals(expected, actual);
    }
}

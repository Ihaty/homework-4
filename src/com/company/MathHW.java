package com.company;

import static java.lang.Math.*;
import static java.lang.Math.log10;

public class MathHW {
    public MathHW() {
        task4();
    }

    public double task1(int v, int a) {
        double speed = v / 3.6;
        return (int) (((speed * speed) / 9.8) * Math.sin(Math.toRadians(2 * speed)));
    }

    public double task1_2(int v, double a) {
        double speed = v / 3.6;
        return (int) (((speed * speed) * sin(2 * a)) / 9.8);
    }

    public int task2(int v1, int v2, int s, int t) {
        return s + t * (v1 + v2);
    }

    public boolean task3(double x, double y) {
        return y <= abs(x) && y >= abs(1.5 * x) - 1;
    }

    public double task4() {
        double x = 5;
        double b = ((6 * log10( sqrt(exp(x+1)) + 2 * exp(x) * cos(x))) / (log10(x - exp(x + 1) * sin(x)))) + abs((cos(x)) / exp(sin(x)));
        return b;
    }
}
